package com.example.mynewaccount.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mynewaccount.R;
import com.example.mynewaccount.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class UserListAdapter extends BaseAdapter {
    Context context;
    ArrayList<HashMap<String,Object>> userlist;
    public UserListAdapter(Context context, ArrayList<HashMap<String,Object>> userlist){
        this.context = context;
        this.userlist = userlist;
    }

    @Override
    public int getCount() {
        return userlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.view_row_user_list,null);
        TextView tvname = view1.findViewById(R.id.txtname);
        TextView tvnumber = view1.findViewById(R.id.txtnumber);
        TextView tvgender = view1.findViewById(R.id.txtgender);

        tvname.setText(userlist.get(i).get(Const.FIRST_NAME)+""+userlist.get(i).get(Const.LAST_NAME));
        tvnumber.setText(String.valueOf(userlist.get(i).get(Const.NUMBER)));
        if (userlist.get(i).get(Const.GENDER).equals("Male")) {
            tvgender.setBackgroundResource(R.drawable.ic_male);
        } else {
            tvgender.setBackgroundResource(R.drawable.ic_female);
        }
        tvgender.setText(userlist.get(i).get(Const.GENDER).equals("Male") ? "M" : "F");
        return view1;
    }
}
