package com.example.mynewaccount;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.mynewaccount.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity2 extends AppCompatActivity {
    EditText txtfirstname,txtlastname,txtemail,txtnumber,txtpassword;
    RadioButton rbmale,rbfemale;
    CheckBox chksinging, chkdancing,chkcooking;
    RadioGroup rggender;
    Button btnsubmit,btnclear;

    ArrayList<HashMap<String,Object>> userlist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        init();


        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Boolean tempCheck = isvalid();
                if (tempCheck) {


                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Const.FIRST_NAME, txtfirstname.getText().toString());
                    map.put(Const.LAST_NAME, txtlastname.getText().toString());
                    map.put(Const.NUMBER, txtnumber.getText().toString());
                    map.put(Const.EMAIL_ADDRESS, txtemail.getText().toString());
                    map.put(Const.Password, txtpassword.getText().toString());
                    map.put(Const.GENDER, rbmale.isChecked() ? rbmale.getText().toString() : rbfemale.getText().toString());
                    String hobbies = "";
                    if (chkcooking.isChecked()) {
                        hobbies += "," + chkcooking.getText().toString();
                    }
                    if (chkdancing.isChecked()) {
                        hobbies += "," + chkdancing.getText().toString();
                    }
                    if (chksinging.isChecked()) {
                        hobbies += "," + chksinging.getText().toString();
                    }
                    if (hobbies.length() > 0) {
                        hobbies.substring(1);
                    }
                    map.put(Const.HOBBY, hobbies);
                    userlist.add(map);
                    Intent intent = new Intent(MainActivity2.this, second.class);
                    intent.putExtra("UserList", userlist);
                    startActivity(intent);

                }

            }
        });

    }
    public void init(){
        txtfirstname = findViewById(R.id.txtfirstname);
        txtlastname = findViewById(R.id.txtlastname);
        txtemail = findViewById(R.id.txtemail);
        txtnumber = findViewById(R.id.txtumber);
        txtpassword = findViewById(R.id.txtpassword);
        rggender = findViewById(R.id.rggender);
        rbmale = findViewById(R.id.rbmale);
        rbfemale = findViewById(R.id.rbfemale);
        chksinging = findViewById(R.id.chksinging);
        chkcooking = findViewById(R.id.chkcooking);
        chkdancing = findViewById(R.id.chkdancing);
        btnsubmit = findViewById(R.id.btnsubmit);
        btnclear = findViewById(R.id.btnclear);
    }
    public Boolean isvalid(){
        Boolean  booleanisvalide = false;

        String email = txtemail.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String MobilePattern = "[0-9]{10}";
        String passwordPattern = "[0-9]{10}";

        if (txtfirstname.getText().toString().isEmpty()){
            txtfirstname.setError("Enter First Name");
            return booleanisvalide;
        }
        if (txtlastname.getText().toString().isEmpty()){
            txtlastname.setError("Enter Last Name");
            return booleanisvalide;
        }


        if (!txtnumber.getText().toString().matches(MobilePattern)){
            txtnumber.setError("Enter Phone Number");
            return booleanisvalide;
        }
        if (!email.matches(emailPattern)) {
            txtemail.setError("Enter valid Email");
            return booleanisvalide;
        }
        if (!txtpassword.getText().toString().matches(passwordPattern)){
            txtpassword.setError("Enter Password");
            return booleanisvalide;
        }
        if (!(chksinging.isChecked() || chkdancing.isChecked() || chkcooking.isChecked())){
            Toast.makeText(getApplicationContext(),"plese choose any one",Toast.LENGTH_SHORT).show();
            return booleanisvalide;
        }
        if (!(rbmale.isChecked() || rbfemale.isChecked())){
            Toast.makeText(getApplicationContext(),"plese choose Gender",Toast.LENGTH_SHORT).show();
            return booleanisvalide;
        }
        Toast.makeText(getApplicationContext(),"Sucessfull ",Toast.LENGTH_SHORT).show();
        return true;
    }
}