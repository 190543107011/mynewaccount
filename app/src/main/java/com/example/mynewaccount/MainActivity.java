package com.example.mynewaccount;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText email,password;
    Button btnlogin,btnregistration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        btnregistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent h = new Intent(getApplicationContext(), MainActivity2.class);
                startActivity(h);
            }
        });

    }
    public void init(){
        email =findViewById(R.id.email);
        password =findViewById(R.id.password);
        btnlogin =findViewById(R.id.btnlogin);
        btnregistration = findViewById(R.id.btnregistration);
    }
}