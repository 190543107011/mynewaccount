package com.example.mynewaccount;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.mynewaccount.adapter.UserListAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class second extends AppCompatActivity {
    ListView listview;
    ArrayList<HashMap<String,Object>> userlist = new ArrayList<>();
    UserListAdapter userListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        init();
        bindviewvalues();
    }
    void bindviewvalues(){
        userlist.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        userListAdapter = new UserListAdapter(this,userlist);
        listview.setAdapter(userListAdapter);

    }

    public void init(){
        listview = findViewById(R.id.listview);
    }
}